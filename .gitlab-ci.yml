variables:
  GIT_STRATEGY: fetch
  GIT_DEPTH: 1000

stages:
  - code_quality
  - test
  - build
  - deploy

.code_quality_template: &code_quality_template
  image: python:3.7
  stage: code_quality
  allow_failure: true
  before_script:
    - make .updated_venv_test~

quality_linter:
  <<: *code_quality_template
  script:
    - make linter

quality_security:
  <<: *code_quality_template
  script:
    - make security

quality_types:
  <<: *code_quality_template
  script:
    - make types
    
# Test template for the various python versions
.test_template: &test_template
  stage: test
  coverage: '/Code coverage: \d+\%/'
  before_script:
    - make .updated_venv_test~
  script:
    - TESTCASES=1000 SLOW_TESTCASES=100 make test
    - make coverage

test_python3.6:
  image: python:3.6
  <<: *test_template
    
test_python3.7:
  image: python:3.7
  <<: *test_template

test_python3.8:
  image: python:3.8
  <<: *test_template

test_python3.9:
  image: python:3.9
  <<: *test_template
  
build_documentation:
  stage: build
  image: python:3.7
  before_script:
    - make .updated_venv_test~
  script:
    - make documentation
  except:
    - master
  artifacts:
    paths:
      - site

deploy_review_documentation:
  stage: deploy
  dependencies:
    - build_documentation
  except:
    - master
    - /^release\/.+/
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: "https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_PATH/-/jobs/$CI_JOB_ID/artifacts/public/index.html"
  script:
    - mv site public
  artifacts:
    paths:
      - public

deploy_staging_documentation:
  stage: deploy
  dependencies:
    - build_documentation
  only:
    - /^release\/.+/
  environment:
    name: staging/$CI_COMMIT_REF_NAME
    url: "https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_PATH/-/jobs/$CI_JOB_ID/artifacts/public/index.html"
  script:
    - mv site public
  artifacts:
    paths:
      - public

pages:
  image: python:3.7
  stage: deploy
  resource_group: pages-production
  before_script:
    - make .updated_venv_test~
  script:
    - make documentation
    - mv site public
  environment:
    name: production
    url: "https://$CI_PROJECT_NAMESPACE.gitlab.io/$CI_PROJECT_NAME"
  artifacts:
    paths:
      - public
  only:
    - master

deploy_pypi_package:
  image: python:3.7
  stage: deploy
  resource_group: pypi-production
  only:
    - /^release\/.+/
  before_script:
    - pip3 install setuptools twine
  script:
    - python3 setup.py sdist build
    - twine check dist/*
    - twine upload dist/*
  artifacts:
    expire_in: never
    paths:
      - dist
