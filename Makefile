.PHONY: test linter security coverage types documentation

SHELL := /bin/bash
PROJECT := $(shell ls */__init__.py | grep -oP '^[^\/]+')

test: .updated_venv_test~
	. venv_test/bin/activate && coverage run --branch --source=$(PROJECT) -m nose2

documentation: .updated_venv_test~
	. venv_test/bin/activate && PYTHONPATH=. mkdocs build --clean

coverage: .updated_venv_test~
	. venv_test/bin/activate && coverage report -m
	@. venv_test/bin/activate && echo -n "Code coverage: " && coverage report -m | tail -n 1 | rev | cut -d' ' -f1 | rev

types: .updated_venv_test~
	. venv_test/bin/activate && mypy $(PROJECT)

linter: .updated_venv_test~
	. venv_test/bin/activate && flake8

security: .updated_venv_test~
	. venv_test/bin/activate && bandit -r $(PROJECT)

program: .updated_venv~
	. venv/bin/activate && python $(PROJECT)/core.py

.updated_venv~: venv requirements.txt
	. venv/bin/activate && pip install -r requirements.txt
	touch .updated_venv~

.updated_venv_test~: venv_test requirements-test.txt requirements.txt
	. venv_test/bin/activate && pip install -r requirements-test.txt && pip install -r requirements.txt
	touch .updated_venv_test~

venv:
	python3 -m venv venv

venv_test:
	python3 -m venv venv_test
