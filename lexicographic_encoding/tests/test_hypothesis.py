from unittest import TestCase
from hypothesis import assume, given, note, settings, strategies as st
from .strategies import all_objects, ordering_strategy
from lexicographic_encoding import (
    is_prefix_of,
    lexpackb, lexunpackb,
    pack_unaryint, unpack_unaryint,
    pack_gammaint, unpack_gammaint,
    pack_deltaint, unpack_deltaint,
    pack_bytes, unpack_bytes,
    pack_string, unpack_string,
    pack_bool, unpack_bool,
    pack_sequence, unpack_sequence,
    pack_none, unpack_none,
    pack_tuple, unpack_tuple,
    pack_list, unpack_list,
    pack_set, unpack_set,
    pack_dict, unpack_dict,
)
from os import getenv


TESTCASES = int(getenv("TESTCASES", 25))
SLOW_TESTCASES = int(getenv("SLOW_TESTCASES", 25))


class TestIdentityEncodingDecoding(TestCase):
    @settings(max_examples=TESTCASES)
    @given(number=st.integers(min_value=-3000, max_value=3000))
    def test_unaryint_identity(self, number):
        note(f"enc {pack_unaryint(number)}")
        self.assertEqual(number, unpack_unaryint(pack_unaryint(number)[0])[0])

    @settings(max_examples=TESTCASES)
    @given(number=st.integers())
    def test_gammaint_identity(self, number):
        note(f"enc {pack_gammaint(number)}")
        self.assertEqual(number, unpack_gammaint(pack_gammaint(number)[0])[0])

    @settings(max_examples=TESTCASES)
    @given(number=st.integers())
    def test_deltaint_identity(self, number):
        note(f"enc {pack_deltaint(number)}")
        self.assertEqual(number, unpack_deltaint(pack_deltaint(number)[0])[0])

    @settings(max_examples=TESTCASES)
    @given(bstring=st.binary())
    def test_bytes_identity(self, bstring):
        note(f"enc {pack_bytes(bstring)}")
        self.assertEqual(bstring, unpack_bytes(pack_bytes(bstring)[0])[0])

    @settings(max_examples=TESTCASES)
    @given(string=st.text())
    def test_string_identity(self, string):
        note(f"enc {pack_string(string)}")
        self.assertEqual(string, unpack_string(pack_string(string)[0])[0])

    @given(b=st.booleans())
    def test_bool_identity(self, b):
        note(f"enc {pack_bool(b)}")
        self.assertEqual(b, unpack_bool(pack_bool(b)[0])[0])

    @settings(max_examples=TESTCASES)
    @given(sequence=st.lists(st.one_of(st.integers(), st.binary(), st.text())))
    def test_onelevel_sequence_identity(self, sequence):
        note(f"enc {pack_sequence(sequence)}")
        self.assertEqual(sequence, unpack_sequence(pack_sequence(sequence)[0])[0])

    @settings(max_examples=TESTCASES)
    @given(sequence=st.tuples(st.one_of(st.integers(), st.binary(), st.text())))
    def test_onelevel_tuple_identity(self, sequence):
        note(f"enc {pack_sequence(sequence)}")
        self.assertEqual(sequence, unpack_tuple(pack_tuple(sequence)[0])[0])

    @settings(max_examples=TESTCASES)
    @given(sequence=st.lists(st.one_of(st.integers(), st.binary(), st.text())))
    def test_onelevel_list_identity(self, sequence):
        note(f"enc {pack_sequence(sequence)}")
        self.assertEqual(sequence, unpack_list(pack_list(sequence)[0])[0])

    @settings(max_examples=TESTCASES)
    @given(dct=st.dictionaries(st.text(), st.one_of(st.integers(), st.binary(), st.text())))
    def test_onelevel_dict_identity(self, dct):
        note(f"enc {pack_dict(dct)}")
        self.assertEqual(dct, unpack_dict(pack_dict(dct)[0])[0])

    @settings(max_examples=TESTCASES)
    @given(st=st.sets(st.one_of(st.integers(), st.binary(), st.text())))
    def test_set_identity(self, st):
        note(f"enc {pack_set(st)}")
        self.assertEqual(st, unpack_set(pack_set(st)[0])[0])

    @given(none=st.none())
    def test_none_identity(self, none):
        self.assertEqual(none, unpack_none(pack_none(none)[0])[0])


class TestOrderPreserved(TestCase):
    @settings(max_examples=TESTCASES)
    @given(a=st.integers(min_value=-3000, max_value=3000),
           b=st.integers(min_value=-3000, max_value=3000))
    def test_unaryint_preserves_order(self, a, b):
        note(f"aenc {pack_unaryint(a)}")
        note(f"benc {pack_unaryint(b)}")
        self.assertEqual(a > b, pack_unaryint(a)[0] > pack_unaryint(b)[0])

    @settings(max_examples=TESTCASES)
    @given(a=st.integers(), b=st.integers())
    def test_gammaint_preserves_order(self, a, b):
        note(f"aenc {pack_gammaint(a)}")
        note(f"benc {pack_gammaint(b)}")
        self.assertEqual(a > b, pack_gammaint(a)[0] > pack_gammaint(b)[0])

    @settings(max_examples=TESTCASES)
    @given(a=st.integers(), b=st.integers())
    def test_deltaint_preserves_order(self, a, b):
        note(f"aenc {pack_deltaint(a)}")
        note(f"benc {pack_deltaint(b)}")
        self.assertEqual(a > b, pack_deltaint(a)[0] > pack_deltaint(b)[0])

    @settings(max_examples=TESTCASES)
    @given(a=st.binary(), b=st.binary())
    def test_bytes_preserves_order(self, a, b):
        note(f"aenc {pack_bytes(a)}")
        note(f"benc {pack_bytes(b)}")
        self.assertEqual(a > b, pack_bytes(a)[0] > pack_bytes(b)[0])

    @settings(max_examples=TESTCASES)
    @given(a=st.text(), b=st.text())
    def test_string_preserves_order(self, a, b):
        note(f"aenc {pack_string(a)}")
        note(f"benc {pack_string(b)}")
        self.assertEqual(a > b, pack_string(a)[0] > pack_string(b)[0])

    @given(a=st.booleans(), b=st.booleans())
    def test_bool_preserves_order(self, a, b):
        note(f"aenc {pack_bool(a)}")
        note(f"benc {pack_bool(b)}")
        self.assertEqual(a > b, pack_bool(a)[0] > pack_bool(b)[0])

    @settings(max_examples=TESTCASES)
    @given(a=st.lists(st.text()), b=st.lists(st.text()))
    def test_onelevel_sequence_string_preserves_order(self, a, b):
        note(f"aenc {pack_sequence(a)}")
        note(f"benc {pack_sequence(b)}")
        self.assertEqual(a > b, pack_sequence(a)[0] > pack_sequence(b)[0])

    @settings(max_examples=TESTCASES)
    @given(a=st.lists(st.lists(st.text())), b=st.lists(st.lists(st.text())))
    def test_twolevel_sequence_string_preserves_order(self, a, b):
        note(f"aenc {pack_sequence(a)}")
        note(f"benc {pack_sequence(b)}")
        self.assertEqual(a > b, pack_sequence(a)[0] > pack_sequence(b)[0])

    @settings(max_examples=TESTCASES)
    @given(a=st.dictionaries(st.text(), st.text()), b=st.dictionaries(st.text(), st.text()))
    def test_onelevel_dictionaries_preserves_order(self, a, b):
        note(f"aenc {pack_dict(a)}")
        note(f"benc {pack_dict(b)}")
        ad = tuple(sorted(a.items(), key=lambda el: el[0]))
        bd = tuple(sorted(b.items(), key=lambda el: el[0]))
        self.assertEqual(ad > bd, pack_dict(a)[0] > pack_dict(b)[0])

    @settings(max_examples=TESTCASES)
    @given(a=st.sets(st.text()), b=st.sets(st.text()))
    def test_sets_preserves_order(self, a, b):
        note(f"aenc {pack_set(a)}")
        note(f"benc {pack_set(b)}")
        ad = tuple(sorted(a))
        bd = tuple(sorted(b))
        self.assertEqual(ad > bd, pack_set(a)[0] > pack_set(b)[0])


class TestPrefixPreserved(TestCase):
    @settings(max_examples=TESTCASES)
    @given(a=st.binary(), b=st.binary())
    def test_binary_preserves_prefix(self, a, b):
        note(f"aenc {pack_bytes(a)}")
        note(f"benc {pack_bytes(b)}")
        self.assertEqual(is_prefix_of(a, b), is_prefix_of(pack_bytes(a)[0], pack_bytes(b)[0]))

    @settings(max_examples=TESTCASES)
    @given(a=st.text(), b=st.text())
    def test_string_preserves_prefix(self, a, b):
        note(f"aenc {pack_string(a)}")
        note(f"benc {pack_string(b)}")
        self.assertEqual(is_prefix_of(a, b), is_prefix_of(pack_string(a)[0], pack_string(b)[0]))

    @settings(max_examples=TESTCASES)
    @given(a=st.lists(st.text()), b=st.lists(st.text()))
    def test_onelevel_sequence_string_preserves_prefix(self, a, b):
        note(f"aenc {pack_sequence(a)}")
        note(f"benc {pack_sequence(b)}")
        self.assertEqual(is_prefix_of(a, b), is_prefix_of(pack_sequence(a)[0], pack_sequence(b)[0]))

    @settings(max_examples=TESTCASES)
    @given(a=st.lists(st.lists(st.text())), b=st.lists(st.lists(st.text())))
    def test_twolevel_sequence_string_preserves_prefix(self, a, b):
        note(f"aenc {pack_sequence(a)}")
        note(f"benc {pack_sequence(b)}")
        self.assertEqual(is_prefix_of(a, b), is_prefix_of(pack_sequence(a)[0], pack_sequence(b)[0]))

    @settings(max_examples=TESTCASES)
    @given(a=st.dictionaries(st.text(), st.text()), b=st.dictionaries(st.text(), st.text()))
    def test_onelevel_dictionaries_preserves_prefix(self, a, b):
        note(f"aenc {pack_dict(a)}")
        note(f"benc {pack_dict(b)}")
        ad = list(sorted(a.items(), key=lambda el: el[0]))
        bd = list(sorted(b.items(), key=lambda el: el[0]))
        self.assertEqual(is_prefix_of(ad, bd), is_prefix_of(pack_dict(a)[0], pack_dict(b)[0]))

    @settings(max_examples=TESTCASES)
    @given(a=st.sets(st.text()), b=st.sets(st.text()))
    def test_sets_preserves_prefix(self, a, b):
        note(f"aenc {pack_set(a)}")
        note(f"benc {pack_set(b)}")
        ad = list(sorted(a))
        bd = list(sorted(b))
        self.assertEqual(is_prefix_of(ad, bd), is_prefix_of(pack_set(a)[0], pack_set(b)[0]))


class TestUserInterface(TestCase):
    @settings(max_examples=SLOW_TESTCASES)
    @given(param=all_objects)
    def test_packing_identity(self, param):
        note(f"packed {lexpackb(param)}")
        self.assertEqual(param, lexunpackb(lexpackb(param))[0])

    @settings(max_examples=SLOW_TESTCASES)
    @given(parameters=ordering_strategy())
    def test_packing_preserves_order(self, parameters):
        a, b = parameters
        note("apacked f{lexpackb(a)}")
        note("bpacked f{lexpackb(b)}")
        assume(a is not None)
        self.assertEqual(a > b, lexpackb(a) > lexpackb(b))

    # TODO: Change ordering_strategy with something that can give also extensions of objects
    @settings(max_examples=SLOW_TESTCASES)
    @given(parameters=ordering_strategy())
    def test_packing_preserves_prefix(self, parameters):
        a, b = parameters
        note(f"aenc {lexpackb(a)}")
        note(f"benc {lexpackb(b)}")
        assume(is_prefix_of(a, b) is not None)
        self.assertEqual(is_prefix_of(a, b), is_prefix_of(lexpackb(a), lexpackb(b)))
