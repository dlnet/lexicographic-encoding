from typing import Tuple
from hypothesis import strategies as st


nonrecursive = st.booleans() | st.integers() | st.binary() | st.text() | st.none()
all_objects = (
    st.sets(nonrecursive) |
    st.recursive(nonrecursive, lambda values: (
        st.dictionaries(nonrecursive, values) |
        st.lists(values) |
        st.tuples(values)
    ), max_leaves=15)
)


@st.composite
def primitive_type(draw):
    """
    Strategy that draws a primitive type.
    Needed to build more complex structured types.
    """
    types = [bool, int, bytes, str, type(None)]
    idx = draw(st.integers(min_value=0, max_value=len(types) - 1))
    return types[idx]


@st.composite
def recurrent_tuple(draw, max_depth=4, max_range=5):
    hasdepth = draw(st.booleans())
    if max_depth <= 0 or not hasdepth:
        return draw(primitive_type())
    else:
        number_of_types = draw(st.integers(min_value=0, max_value=max_range))
        tuple_types = tuple(draw(recurrent_tuple(max_depth=max_depth-1, max_range=max_range))
                            for _ in range(number_of_types))
        return Tuple[tuple_types]


@st.composite
def ordering_strategy(draw, **kwargs):
    list_type = draw(recurrent_tuple(**kwargs))
    strat = st.from_type(list_type)
    return draw(strat), draw(strat)
