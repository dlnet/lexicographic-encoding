from unittest import TestCase
from lexicographic_encoding import UnknownClassEncoder, UnknownClassDecoder, lexpackb, lexunpackb


class TestMisbehaviour(TestCase):
    def test_unknown_encoder(self):
        self.assertRaises(UnknownClassEncoder, lambda: lexpackb(None, mapping={}))

    def test_unknown_decoder(self):
        self.assertRaises(UnknownClassDecoder, lambda: lexunpackb(b"\x00", mapping={}))
