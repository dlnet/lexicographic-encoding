# How to Contribute
We are glad you want to contribute!
We welcome any kind of contribution from anyone.

There are many ways in which you can contribute to the project: writing code, writing documentation (which is often
more important than code), answering questions of other users or donating to the project.

In case you want to donate, please refer to the information published in the donation section in the documentation.

## Question Answering
You can freely answer questions on the issues of this repository and on other used mediums.
If a question is asked on another medium and the answer cannot easily be found on the current documentation,
it would be great if you open an issue in this repository to signal it to us.

## Code and Documentation
### Contribution Flow
For what concerns writing code or documentation the flow is similar:
1. Open an issue in the current repository (be sure to select the right template).
2. Wait for some feedback about how to move in your contribution and specific guidelines.
   Please note that in case of code contribution you are also required to add or change the parts of
   the documentation that refer to the part of your code.
3. Write code / documentation and open a Merge Request to the current report.
4. Wait for comments on your production and answer to them or modify you work.
5. Eventually the Merge Request will get approved and you will be inserted in the list of contributors.

### Code Writing Guidelines
We try to adhere to common practices for writing readable code.
You can find fixes for adherence to PEP8 guidelines in your contribution by running `make linter`,
possible logical errors in type checking by running `make types`,
and common security vulnerabilities in code by `make security`.

Moreover we require that all code contributions come with unit tests with 100% coverage.

### Documentation Writing Guidelines
Since there are no automatic tools to check for documentation quality, it is a more optional matter.
A useful checklist for your work is:
1. The documented content is technically correct, consistent, complete and is easily understood
2. The document format and style is consistent with similar documentation
3. Links to internal or external references are always included when relevant
4. Each page starts with a section of the necessary background to be able to understand it
5. Graphic content includes information to introduce it
6. Spell and Grammar are correct

After writing the part of the documentation, please read it twice to be sure that the relevant concepts
are easily conveyed to a person reading it.
