# Lexicographic Encoding

## Version 0.1.1
- Add encoding of sets and dictionaries

## Version 0.1.0
- Add encoding of nested sequences
- Support for encoding of integers, bytes, strings, booleans, nones


