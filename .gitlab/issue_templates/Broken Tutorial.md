# BROKEN TUTORIAL
<!-- Use this template if some code for a tutorial does not directly work as provided. -->

<!--
_Tutorials help users to begin using our software, and thus help in expanding the community._
_They are thus very important to us!_
_We thank you for taking the time to properly fill the relevant information in the template._
-->

### Summary
<!--
Summarize concisely what the question is about.
Link also the relevant tutorial or guide in official documentation.
-->

### Software Informations
<!-- Please provide informations about the software you are using, if applicable: -->
- Python version: FILL
- Package version: FILL
- Operating system: FILL

### Custom Modifications and Additional Information about the environment
<!--
Have you made any custom modification to the software or to the standard operational setting
in which the software has been run?

Also provide any additional information about your setup you deem relevant to possibly solving the issue.
-->

### Steps to reproduce
<!--
How one can reproduce the issue - VERY IMPORTANT!
Be sure to include all details and possibly a step by step procedure to follow for reproducing.
Include a link to the relevant documentation.
-->

### Code Snippet
<!-- Copy-Paste the relevant code example that is not working properly. -->

### Current Behaviour
<!-- Please describe what happens currently. -->

### Expected Behaviour
<!-- Please describe the expected behaviour of the code snippet. -->

### Possible fixes
<!--
If you can, describe ways in which the tutorial can be changed to fix the current issue.
If applicable, link to the line of code in the codebase that might be responsible for the problem.
-->

### Additional Details
<!--
Provide any additional detail that is relevant to the issue.

Also mention if you want to submit a Merge Request for the issue, and an ETA for it.
In this case, please be sure to include any concerns that you have or questions about some obscure details to you
and please wait for feedback before starting to work: we don't want to waste your time!
-->

### Other links / references
<!-- E.g. related issues/MRs or external links -->

<!--
_Thanks for taking the time to report the issue._
_This means a lot to us, and we will try to provide the fastest feedback._

_Have a nice day!_
-->
