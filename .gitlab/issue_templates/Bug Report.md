# BUG REPORT
<!-- Use this template if something is not working as it should in the software. DO NOT USE FOR VULNERABILITIES. -->

<!--
PLEASE DO NOT USE THIS TEMPLATE FOR VULNERABILITY REPORT, USE THE APPROPRIATE ONE.
-->

<!--
_Thanks for taking your time to report a bug. We appreciate it!_
_Please fill the relevant information in the template to help us fix the bug timely._
-->

<!-- PLEASE SEARCH FIRST FOR SIMILAR QUESTIONS IN OUR DOCUMENTATION AND IN OTHER ISSUES -->

### Summary
<!-- Summarize the bug encountered concisely. -->

### Software Informations
<!-- Please provide informations about the software that you are using: -->
- Python version: FILL
- Package version: FILL
- Operating system: FILL

### Custom Modifications and Additional Information about the environment
<!--
Have you made any custom modification to the software or to the standard operational setting in which the software has been run?

Also provide any additional information about your setup you deem relevant to possibly solving the issue.
-->

### Steps to reproduce
<!--
How one can reproduce the issue - VERY IMPORTANT!
Be sure to include all details and possibly a step by step procedure to follow for reproducing.

If the bug happens only sometimes by following the procedure, provide a rough estimate of the percentage of cases.
-->

### Minimal Working Example
<!--
Please paste a minimal script that reproduces the bug or link to a GitHub / GitLab project or snippet that allows to reproduce the bug.
-->

### Current Bug Behaviour
<!-- Please describe what happens currently. -->

### Expected Behaviour
<!-- Please describe the expected behaviour. -->

### Relevant logs and/or screenshots
<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's very hard to read otherwise. -->

### Additional Diagnostic Information
<!-- Provide additional diagnostic information that can help in solving the bug. -->

### Possible fixes
<!-- If you can, link to the line of code in the codebase that might be responsible for the problem. -->

### Additional Details
<!--
Provide any additional detail that is relevant to the issue.

Also mention if you want to submit a Merge Request for the issue, and an ETA for it.
In this case, please be sure to include any concerns that you have or questions about some obscure details to you
and please wait for feedback before starting to work: we don't want to waste your time!
-->

### Other links / references
<!-- E.g. related issues/MRs or external links -->

<!--
_Thanks for taking the time to fill the bug report._
_This means a lot to us, and we will try to provide the fastest feedback on the issue._

_Have a nice day!_
-->
