# GENERAL QUESTION
<!-- Use this template if you want to ask a non-coding question. -->

<!-- PLEASE SEARCH FIRST FOR SIMILAR QUESTIONS IN OUR DOCUMENTATION AND IN OTHER ISSUES -->

<!--
_Questions help us defining what part of our software are the most obscure._
_We thank you for taking the time to properly fill the relevant information in the template._
-->

### Summary
<!-- Summarize concisely what the question is about or what you want to archieve. -->

### Software Informations
<!-- Please provide informations about the software you are using, if applicable: -->
- Python version: FILL
- Package version: FILL
- Operating system: FILL

### Custom Modifications and Additional Information about the environment
<!--
Have you made any custom modification to the software or to the standard operational setting
in which the software has been run?

Also provide any additional information about your setup you deem relevant to possibly solving the issue.
-->

### Questions
<!--
State the questions you have, clearly exposing what part you don't understand or need clarifications about,
what behaviour do you expected from such parts, and what you want to archieve or know.

If you have multiple non-connected questions it is better to ask them separately in different issues.

Be sure to read this section twice after writing it to be sure that the message has been conveyed clearly and that
the question can be understood by another person.
-->

### Additional Details
<!--
Provide any additional detail that is relevant to the issue.

Also mention if you want to submit a Merge Request for the documentation if the issue is solved.
-->

### Other links / references
<!-- E.g. related issues/MRs or external links -->

<!--
_Thanks for taking the time to fill the template._
_We will try to provide the fastest feedback on your question._

_Have a nice day!_
-->
