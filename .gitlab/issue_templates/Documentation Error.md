# DOCUMENTATION ERROR
<!-- Use this template if there is an error in the documentation or it is not clear in a certain part. -->

<!--
_We are glad that you use our software (or want to)!_
_Please fill the relevant information in the template to help us._
-->

<!-- PLEASE SEARCH FIRST FOR SIMILAR QUESTIONS IN OTHER ISSUES -->

### Problem to solve
<!--
Summarize concisely what is the problem, and what part of the documentation is affected (include links or paths)
-->

### Further details
<!-- Include use cases, benefits, and/or goals for this work. If applicable, what audience is it intended for? -->

### Proposal
<!-- Further specifics for how can we solve the problem, if needed. -->

### Additional Details
<!--
Provide any additional detail that is relevant to the issue.

Also mention if you want to submit a Merge Request for the issue, and an ETA for it.
In this case, please be sure to include any concerns that you have or questions about some obscure details to you
and please wait for feedback before starting to work: we don't want to waste your time!
-->

### Who can address the issue
<!-- Is some kind of special expertise required to solve it? -->

### Other links / references
<!-- E.g. related issues/MRs or external links -->

<!--
_Thanks for taking the time to fill the template._
_We will try to provide the fastest feedback on the request._

_Have a nice day!_
-->
