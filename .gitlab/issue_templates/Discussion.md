# DISCUSSION
<!--
Use this template if you think that something needs to be discussed with the community for guidance or ideas.
If you have a well-defined question that you think can be well-addressed by a specific person,
please use the template "General Question".
-->

### Summary
<!-- Summarize concisely what the discussion is about. -->

### Accurate Description
<!--
Describe more precisely the discussion object and state the important questions, clearly exposing what part you
need clarifications about, or what behaviour is expected, what we are trying to solve?
-->

### Discussion Goal
<!--
Describe what is the final goal of the discussion, i.e. when it can be considered completed.
E.g. Certain implementation details are agreed upon, a clear idea of how to do something is archived.
-->

### Initial Ideas and Possible Proposals
<!--
If you have though about it, please describe what ideas you have had.
This is also the place to write possible proposals that address (at least partially) the goal of the discussion.

Be sure to clearly separate what the different ideas or proposals are about, and for each one try to also highlight
its strenghts and weaknesses, so that they can be later addressed by other participants.
If possible give a short and meaningful title to each proposal, so that they can later be better referred to.
-->

### Additional Details
<!-- Provide any additional detail that is relevant to the issue. -->

### Other links / references
<!-- E.g. related issues/MRs or external links -->

<!--
_Thanks for taking the time to fill the template._

_Have a nice day!_
-->
