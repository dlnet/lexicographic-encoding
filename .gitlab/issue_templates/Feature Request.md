# FEATURE REQUEST
<!-- Use this template if you want to request or implement a new feature in the software. -->

<!-- PLEASE SEARCH FIRST FOR SIMILAR QUESTIONS IN OUR DOCUMENTATION AND IN OTHER ISSUES -->

<!--
_We are glad that you want to suggest possible improvements to the software._
_Please fill the relevant information in the template to help us understand your idea!_
-->

### Summary
<!-- Summarize the requested feature concisely. -->

### Use cases
<!--
Mention appropriate use cases of the new feature which are not addressed by the current setup
or that would be better addressed by the new feature.

Be sure to provide ideal code snippets for what would be the usage of the new feature.
-->

### Dependencies and Deprecations
<!--
Specify on which other features it depends on and which other features 
it would render useless (either already existing or just proposed).
-->

### Implementation Details
<!--
If you also have an implementation proposal, please write it here as accurate as possible.
In particular, be sure to address which part of the codebase would have to be changed / refactored.

If you do not have a clear idea yet you can leave this blank.
-->

### Possible alternatives
<!-- If the use cases can be addressed in other ways, please elaborate on them. -->

### Incremental Steps
<!--
If the feature can be implemented in multiple steps, each of which already brings some benefits, is fully testable
and requires less work than a full-blown implementation, please write them here as a todolist.
-->

### Additional Details
<!--
Provide any additional detail that is relevant to the issue.

Also mention if you want to submit a Merge Request for the issue, and an ETA for it.
In this case, please be sure to include any concerns that you have or questions about some obscure details to you
and please wait for feedback before starting to work: we don't want to waste your time!
-->

### Other links / references
<!-- E.g. related issues/MRs or external links -->

<!--
_Thanks for taking the time to fill the template._
_We will try to provide the fastest feedback on the request._

_Have a nice day!_
-->
