# CODE USAGE QUESTION
<!--
Use this template if you have a problem in getting a piece of code to work or
you don't understand how to use a piece of the current software
-->

<!--
_We are glad that you use our software (or want to)!_
_Please fill the relevant information in the template to help us help you!_
-->

<!-- PLEASE SEARCH FIRST FOR SIMILAR QUESTIONS IN OUR DOCUMENTATION AND IN OTHER ISSUES -->

### Summary
<!-- Summarize concisely what the code is about and what you want to archieve. -->

### Software Informations
<!-- Please provide informations about the software you are using: -->
- Python version: FILL
- Package version: FILL
- Operating system: FILL

### Custom Modifications and Additional Information about the environment
<!--
Have you made any custom modification to the software or to the standard operational setting in which the software has been run?

Also provide any additional information about your setup you deem relevant to possibly solving the issue.
-->

### Concerned part of Software
<!-- If applicable provide what part of the code you are trying to use (class / function) or are asking the question about -->

### Current Code
<!-- If applicable please provide a minimal snippet of code that you have or what you tried to do -->

### Current Behaviour
<!-- Explain what problems you are having with the current code behaviour -->

### Wanted Behaviour
<!-- Please describe the behaviour you would like the code to have -->

### Relevant Diagnostic Information
<!-- Provide logs if relevant and additional diagnostic information that can help in solving the bug. -->

### Additional Details
<!--
Provide any additional detail that is relevant to the issue.

Also mention if you would like to submit a Documentation Merge Request if the issue is solved.
-->

### Other links / references
<!-- E.g. related issues/MRs or external links -->

<!--
_Thanks for taking the time to fill the template._
_We will try to provide the fastest feedback on your question._

_Have a nice day!_
-->
