# TOOLS AND PIPELINE
<!-- Use this template to suggest changes or highlight problems with the used tools and pipeline -->

<!-- PLEASE SEARCH FIRST FOR SIMILAR QUESTIONS IN OUR DOCUMENTATION AND IN OTHER ISSUES -->

<!--
_Please fill the relevant information in the template._
-->

### Problem to solve
<!-- Summarize concisely what is the problem with the current setup, and what part of the environment is affected -->

### Further details
<!-- Include use cases, benefits and/or goals for this work. If applicable, what audience is it intended for? -->

### Proposal
<!--
Further specifics for how can we solve the problem.
In case of tool suggestion provide a little comparison between the currently used one and the proposed one.
Be sure to address suggested workflows if applicable, easyness of installation and usage, 
-->

### Additional Details
<!--
Provide any additional detail that is relevant to the issue.
This is a good place to insert personal experience with the tools.

Also mention if you want to submit a Merge Request for the issue, and an ETA for it.
In this case, please be sure to include any concerns that you have or questions about some obscure details to you
and please wait for feedback before starting to work: we don't want to waste your time!
-->

### Other links / references
<!-- E.g. related issues/MRs or external links -->

<!--
_Thanks for taking the time to fill the template._
_We will try to provide the fastest feedback on the request._

_Have a nice day!_
-->
