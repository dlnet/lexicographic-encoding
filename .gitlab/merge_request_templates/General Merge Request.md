<!--
_Thanks for taking your time to improve the project!_
_Please fill the relevant information in the template to help us review the Merge Request as fast as possible._
-->

<!-- PLEASE CHECK THAT ALL DOCUMENTATION HAS BEEN PROPERLY UPDATED TO REFLECT THE CHANGES -->

### Summary
<!---
Summarize concisely what does the Merge Request solve.
Provide links to the relevant issues, and mention if they would be solved or partially advanced by such merge request.
-->

### Detailed description of the changes
<!--
Provide a more in-detail description of the changes to help us review them - VERY IMPORTANT!
Start with a more general overview of the change and then make clear relevant changes in each
file or part of code or documentation.

Each commit should deal with a different aspect and should represent a working version of the code
in itself. Please provide a brief summary about what has been done specifically in each commit.
-->

### Parts which need to be discussed
<!--
Write which parts of the contribution you are unsure about or need to be discussed in some way.
Please write clear full-sentence questions so that we may answer them fully.
-->

### Additional Details
<!-- Provide any additional detail that is relevant to the issue. -->

### Other links / references
<!-- E.g. related issues/MRs or external links -->

<!--
_Thanks for taking the time to improve the code._
_This means a lot to us, and we will provide the fastest feedback on the merge request._

_Have a nice day!_
-->
