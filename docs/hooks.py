# flake8: noqa W293
from mkdocs.structure.files import Files, File
from fnmatch import fnmatch


def autodoc_content(pathname):
    """
    Returns a standard autodocumentation page for the page name.
    """
    # pathname = autodoc_*.md
    # We extract the middle part
    thisclass = pathname[8:-3].replace("_", ".").replace("-", "_")
    result = f"""
    ---
    title: {thisclass}
    tags:
      - autodoc
    ---

    # Autodoc: {thisclass}
    
    ::: {thisclass}
    
    """
    return result
    

aliases = {
    "index.md": "README.md",
    "license.md": "LICENSE",
    "autodoc_*.md": autodoc_content,
}


def recursive_get_values(dct):
    KEYS = []
    if type(dct) == list:
        lst = dct
        dct = {}
        [dct.update(subdct) for subdct in lst]
    for key, value in dct.items():
        if type(value) in [dict, list]:
            KEYS += recursive_get_values(value)
        else:
            for pathspec, content in aliases.items():
                if fnmatch(value, pathspec):
                    KEYS.append(value)
                    break
    return KEYS


def add_aliased_files(files, config):
    """
    Add files that have been aliased, and mentioned in the nav to the list of files.
    """
    # First remove files with path ending in ~ or hooks.py
    flist = [file for file in files if not file.src_path.endswith("~") and not file.src_path == "hooks.py"]
    aliasedfiles = recursive_get_values(config["nav"])
    for apath in aliasedfiles:
        flist += [File(apath, config["docs_dir"], config["site_dir"], config["use_directory_urls"])]
    return Files(flist)


def read_aliased_files(page, config):
    """
    Instead of some files, captures the content of others.
    """
    srcpath = page.file.src_path
    for pathspec, content in aliases.items():
        if fnmatch(srcpath, pathspec):
            if isinstance(content, str):
                return open(content).read()
            elif callable(content):
                # If the alias is a function, we call it with the path
                return content(srcpath)
