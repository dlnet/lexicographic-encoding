# Lexicographic Encoding - Quick Start
We are glad that you want to use this beautiful project.
This short introduction is here to help you get started quickly.

## What problem does the encoding solve?
Imagine you are trying out the newest key-value and would like to store some sort of index in them.
The interface they offer is pretty poor though:
```python
def put(key: bytes, value: bytes):                             # Store some bytes under a key
def get(key: bytes) -> bytes:                                  # Read the content of a key, if it exists
def delete(key: bytes):                                        # Remove the content associated with a key
def range(start_key: bytes, end_key: bytes) -> List[bytes]:    # Returns the list of key in the range
```

You say: no problem, I can create my index by inserting keys, which are ordered and thus searchable
using the range iterator. This is good, except that it only works with byte keys, so it is pretty good
for sorting strings, but sucks if you try to insert encoded integers instead.

Infact, you can easily see why:
```python3
>>> "102" < "20"
True

>>> "-1" < "-2"
True
```

So what do you do now?
Either you tell your boss that your fantastic banking app can sort transfers by sender but not by value, or
you better find a solution to that problem.

Let's try to address the first problem, namely the fact that if the length of the representations in digits
is different, you end up comparing the dozens with the units.

You think: no problem! It is enough to zero-pad balances up to the millionth.
We end up wasting a tiny bit of space but we won't have any problem with this anymore:
```python
>>> "000102" < "000020"
False
```
Wonderful! But the problem concerning negative numbers remains.
And what if tomorrow someone has a balance of more than a million?

Depending on how farsighted you are either it ends up being poorer that someone with just a couple of hundred
thousands or it gets an error in your application, and you have to manually apply a "migration" of your database
to keep it working for another twenty years, just when [inflation](https://en.wikipedia.org/wiki/Inflation) catchs up.

You probably understand where this is going.
Now imagine if you later want to know, for each person name, who is the richest with that name.
Probably the most efficient thing would be to first order balances for richness with your shiny numeric index, and
later iterate over it to find the richest person with every name.

If you had a multi-level index instead, i.e. one that first orders by person name and then by richness, you would
already have the answer with a couple of very simple lookup (well, not really, but the complexity is definitely
simpler).

The good news is: this package allows you to do exactly this!
It encodes tuples of data (strings and integers mainly) in a bytestring in a way to preserve the
[lexicographic order](https://en.wikipedia.org/wiki/Lexicographical_order) of tuples and also a
prefix relation (more on this later).

Let's start with some code though!

## Usage examples
Remember to import the useful functions (the interface is similar to that of [msgpack](http://msgpack.org/)):
```python
from lexicographic_encoding import lexpackb, lexunpackb
```

Then you can easily use the pack and unpack functions to encode and decode nested tuples into byte sequences:
```python
>>> lexpackb(18)
b'\x01\xc0\x01\x12'

>>> lexpackb(-892)
b'\x01?\xfd\xfc\x83'

>>> lexpackb("a short string")
b'\x03a short string'

>>> lexpackb(["Tom", 1009, False])
b'\x06\xa7\x03Tom\x00\x00\xa7\x01\xc0\x02\x03\xf1\xa7\x04\x00'

>>> lexpackb(["Tom", [45, 18], [], True])
b'\x06\xa7\x03Tom\x00\x00\xa7\x06\xa7\x01\xc0\x01-\xa7\x01\xc0\x01\x12\x00\xa7\x06\x00\xa7\x04\x01'
```

### What does the encoding exactly preserves?

Of course it is invertible and we always have:
```python
assert lexunpackb(lexpackb(nested_tuple)) == nested_tuple
```

Then it also respects the ordering on integers, strings and tuples:
```python
(a > b) == (lexpackb(a) > lexpackb(b))
```

In particular this means the following
```python
>>> lexpackb(8) > lexpackb(2)
True

>>> lexpackb(-1) > lexpackb(-2)
True

>>> lexpackb(102) > lexpackb(20)
True

>>> lexpackb((102, 4, 6)) > lexpackb((20, 8, 6))
True

>>> lexpackb((102, 4, 6)) > lexpackb((102, 8, 6))
False
```

But it also has another property, namely it preserves strings- and tuples-prefixes, that can be used to create
subdatabases inside the main one and to efficiently search using range queries.

Prefix preserval means that if two sequences (tuples, strings and byte sequences) share an initial part, their
encodings will share it too!
```python
>>> lexpackb((20, "Tom"))
b'\x05\xa7\x01\xc0\x01\x14\xa7\x03Tom'

>>> lexpackb((20, "Tommy"))
b'\x05\xa7\x01\xc0\x01\x14\xa7\x03Tommy'
```
This helps because now to search for every key that starts with `(20, "Tom")` you can issue a range query:
```python
encoded = lexpackb((20, "Tom"))
range(start_key=encoded, end_key=encoded + bytes([255]))
```
The only caveat you should pay attention to is that with the above search you will also obtain results that extend
the word "Tom" in the second position, and then have whatever else, like `(20, "Tommy", 18)`.

If you have a level of the hierarchy that you only want to query exactly (for example for splitting the database
to create subdatabases), you should insert a blocking identifier after it, for example if you want to get only
records in the personal database of Tom, you can insert keys as `(20, "Tom", None, ___)` so that when you search
the range with prefix `(20, "Tom", None)` you only get results that exactly match until that point.

## Conclusions
Now you have all the tools to solve the key-value storage problem. Enjoy!

If you still have questions that the current documentation does not answer,
please [ask them](https://gitlab.com/dlnet/lexicographic-encoding/-/issues/).
Thank you for following along!

You may now like to check the autogenerated documentation for functions (which includes the strategy with which
the data is encoded) in this site or go straight to [read the code](https://gitlab.com/dlnet/lexicographic-encoding/).
