# Docstrings
This page lists the implemented functions of the package so that the user can use them in custom encoding methods.

## User Interface

::: lexicographic_encoding.lexpackb

::: lexicographic_encoding.lexunpackb

::: lexicographic_encoding.is_prefix_of

::: lexicographic_encoding.PACK_MAPPING

::: lexicographic_encoding.UNPACK_MAPPING

## Raised Exceptions

::: lexicographic_encoding.UnknownClassEncoder

::: lexicographic_encoding.UnknownClassDecoder

## Composable Methods

### Packing

::: lexicographic_encoding.pack_unaryint

::: lexicographic_encoding.pack_gammaint

::: lexicographic_encoding.pack_deltaint

::: lexicographic_encoding.pack_bytes

::: lexicographic_encoding.pack_string

::: lexicographic_encoding.pack_bool

::: lexicographic_encoding.pack_sequence

::: lexicographic_encoding.pack_tuple

::: lexicographic_encoding.pack_list

::: lexicographic_encoding.pack_set

::: lexicographic_encoding.pack_dict

::: lexicographic_encoding.pack_none

::: lexicographic_encoding.pack_general

### Unpacking

::: lexicographic_encoding.unpack_unaryint

::: lexicographic_encoding.unpack_gammaint

::: lexicographic_encoding.unpack_deltaint

::: lexicographic_encoding.unpack_bytes

::: lexicographic_encoding.unpack_string

::: lexicographic_encoding.unpack_bool

::: lexicographic_encoding.unpack_sequence

::: lexicographic_encoding.unpack_tuple

::: lexicographic_encoding.unpack_list

::: lexicographic_encoding.unpack_set

::: lexicographic_encoding.unpack_dict

::: lexicographic_encoding.unpack_none
